import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import 'element-theme-default'; 
import './css/styles.css';

ReactDOM.render(<App />, document.getElementById('root'));
