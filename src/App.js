import React, { Component } from 'react';
import { InstantSearch } from 'react-instantsearch/dom';

import Content from './components/Content';

class App extends Component {
  render() {
    return (
      <InstantSearch appId='TJYHMJ0D9U' apiKey='c42cbce5b011df424ad98393d4aaf410' indexName='ikipetall_es'>
        <Content/>
      </InstantSearch>
    );
  }
}

export default App;
