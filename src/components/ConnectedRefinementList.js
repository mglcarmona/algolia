import React from 'react';
import { connectRefinementList } from 'react-instantsearch/connectors';
import { Checkbox } from 'element-react';

const RefinementList = ({ items, refine }) =>
  <Checkbox.Group onChange={e => refine(e)} style={{ display: 'flex', flexDirection: 'column' }} >
    {
      items.map(({ label }) => (
        <Checkbox label={ label } key={ label } style={styles.listItem} />
      ))
    }
  </Checkbox.Group>

const styles = {
  listItem: {
    margin: 0,
    marginTop: 5,
    marginBottom: 5,
    color: '#20A0FF'
  }
}

export default connectRefinementList(RefinementList);