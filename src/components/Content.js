import React from 'react';
import { Hits, SearchBox, ScrollTo } from 'react-instantsearch/dom';
import { CSSTransitionGroup } from 'react-transition-group' // ES6

import { Layout } from 'element-react';

import Hit from './Hit';
import ConnectedPagination from './ConnectedPagination';
import ConnectedRefinementList from './ConnectedRefinementList';
import Media from 'react-media'

export default class Content extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isOpen: false
    }
  }
  render () {
    return (
      <main>
        
        
        <Media query="(min-width: 770px)">
          {matches => matches ? (
            <Layout.Col xs="24" className="sidebar show" style={{padding: 20}}>
              <h5>Marcas</h5>
              <ConnectedRefinementList attributeName="manufacturer" />
            </Layout.Col>
          ) : (
            <CSSTransitionGroup
              transitionName="example"
              transitionEnterTimeout={300}
              transitionLeaveTimeout={300}
            >
              {
                this.state.isOpen &&
                  <Layout.Col xs="24" className="sidebar" style={{padding: 20}}>
                    <h5>Marcas</h5>
                    <ConnectedRefinementList attributeName="manufacturer" />
                  </Layout.Col>
              }
            </CSSTransitionGroup>
          )}
        </Media>

        <Layout.Col xs="24" className="content" type="flex" justify="center">
          <Layout.Row xs="24" type="flex" justify="center" style={{ marginTop: 20 }}>
            <SearchBox/>
            <button className="filter" onClick={() => this.setState({ isOpen: !this.state.isOpen })} >
              Filtrar
            </button>
          </Layout.Row>

          <Layout.Row xs="24" type="flex" justify="center" style={{ marginTop: 20, marginBottom: 10 }}>
            <ConnectedPagination/>
          </Layout.Row>

          <Layout.Row xs="24">
            <ScrollTo>
              <Hits hitComponent={Hit} ></Hits>
            </ScrollTo>
          </Layout.Row>

          <Layout.Row span="24" type="flex" justify="center" style={{ marginTop: 10, marginBottom: 25 }}>
            <ConnectedPagination/>
          </Layout.Row>
        </Layout.Col>
      </main>
    );
  }
}