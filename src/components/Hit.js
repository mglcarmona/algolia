import React from 'react';

const Hit = ({ hit }) =>
  <div className="card">
    <img src={ `http://${hit.image_link_small}` } alt="" style={ styles.img }/>
    <span>{ hit.name }</span>
    <span className="blue" >${ parseInt(hit.price, 10) }.00</span>
    <button className="primary">Comprar!</button>
    <button>Leer más</button>
  </div>

const styles = {
  img: {
    width: 100,
    height: 100, 
    objectFit: 'cover'
  }
};

export default Hit;