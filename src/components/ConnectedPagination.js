import React from 'react';
import { connectPagination } from 'react-instantsearch/connectors';
import { Pagination } from 'element-react';


const MyPagination = ({ refine, nbPages }) =>
  <Pagination 
    layout="prev, pager, next"
    total={ nbPages }
    small={true}
    onCurrentChange={e => refine(e)} />



const ConnectedPagination = connectPagination(MyPagination);

export default ConnectedPagination;